package base;

/**
 * 转人民币大写
 * e.g. 12.34 -> 拾贰元叁角肆分
 */
public class RMBUpperCase {
    private String[] hanArr = {"零", "壹", "贰", "叁", "肆", "伍", "陆", "柒", "捌", "玖"};
    private String[] unitArr = {"", "拾", "佰", "仟"};
    private String[] unitArrs = {"万", "亿", "万", "兆", "万", "亿", "万", "圆"};

    public String solution(String money) {
        //判断小数后几位
        String[] s = money.split("\\.");
        int len = s[1].length();
        if (len > 2) {
            return "小数错误";
        }
        //转换钱string->double
        Double number = Double.valueOf(money);
        return toHanStr(number);
    }

    //处理字符串
    private String[] divideNum(Double number) {
        double num = Math.round(number * 100);
        long integerPart = (long) num;
        String decimalsPartStr;
        long b = integerPart % 10;//最后一位
        long a = (integerPart / 10) % 10;//倒数第二位
        integerPart /= 100;
        if (a == 0 && b == 0) {
            decimalsPartStr = null;
        } else {
            decimalsPartStr = "" + a + b;
        }
        return new String[]{String.valueOf(integerPart), decimalsPartStr};
    }

    //
    public String toHanStr(Double number) {
        String[] results = new String[9];
        String[] resultStrs = new String[9];
        String result = "";//最终的转换结果
        String[] divideStr = divideNum(number);
        results[8] = divideStr[1];
        for (int i = divideStr[0].length(), j = 8; i > 0 && j > 0; i -= 4, j--) {
            try {
                results[j - 1] = divideStr[0].substring(i - 4, i);
            } catch (Exception e) {
                results[j - 1] = divideStr[0].substring(0, i);
                break;
            }
        }
        if (results[8] == null) {
            resultStrs[8] = "整";
        } else if (results[8].charAt(1) == '0') {
            resultStrs[8] = hanArr[results[8].charAt(0) - 48] + "角";
        } else {
            resultStrs[8] = hanArr[results[8].charAt(0) - 48] + "角" + hanArr[results[8].charAt(1) - 48] + "分";
        }
        for (int i = 0; i < 8; i++) {
            if (results[i] != null) {
                resultStrs[i] = "";
                resultStrs[i] += hanArr[results[i].charAt(0) - 48] + unitArr[results[i].length() - 1];
                // 选单位
                for (int j = 1; j < results[i].length(); j++) {
                    if (results[i].charAt(j - 1) == '0' && results[i].charAt(j) != '0') {
                        resultStrs[i] += "零" + hanArr[results[i].charAt(j) - 48] + unitArr[results[i].length() - 1 - j];

                    } else if (results[i].charAt(j) != '0') {
                        resultStrs[i] += hanArr[results[i].charAt(j) - 48] + unitArr[results[i].length() - 1 - j];

                    }
                }
            }
        }
        for (int i = 0; i < 8; i++) {
            if (resultStrs[i] != null) {
                result += resultStrs[i] + unitArrs[i];
            }
        }
        result += resultStrs[8];
        return result;
    }
}

