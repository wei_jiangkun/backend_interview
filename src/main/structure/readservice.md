### 背景

小打卡首页每日有近亿级别的日记曝光给用户，为了不推荐给用户重复的内容，需要记录每个用户的已读记录，
这些记录可能要保存很长时间（至少1年）。等用户下次拉去feed流时，推荐系统会将该用户已读的日记过滤掉，
请设计一个尽可能高性能的服务，提供稳定，高效的过滤服务同时考虑系统扩展性。

```
##此处作答

一、直查数据库百万级数据秒回
1.建立用户表user和一个日志记录表log
2.用户表存一个已读记录的id集合
3.每次推荐记录时，首先查询该用户的已读记录id的集合
4.根据记录id集合和用户账号查询log表（log表user_id已加索引，所以根据id和user_id查询可以使用索引，速度较快）
由于记录数据量十分巨大可以考虑将记录表根据记录里面的具体内容进行分表，表之间通过主外键连接
查询时进行一次表的左连接
DDL:
user表
CREATE TABLE `xdk_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `nick_name` varchar(255) DEFAULT NULL COMMENT '小打卡账户昵称',
  `uuid` varchar(255) DEFAULT NULL COMMENT '小打卡账户',
  `has_read_id_list` varchar(255) DEFAULT NULL COMMENT '已读记录id集合',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
log表
CREATE TABLE `xdk_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `text` text NOT NULL COMMENT '日志内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `modified_time` datetime DEFAULT NULL COMMENT '修改日期',
  `creator` varchar(255) DEFAULT NULL COMMENT '创建者小打卡昵称',
  `user_id` varchar(255) DEFAULT NULL COMMENT '小打卡账户',
  `is_del` varchar(255) DEFAULT NULL COMMENT '是否删除（用于定时任务删除日志）',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

二、带有搜索引擎，可以实现亿量数据的秒回
如果整合搜索引擎的话可以在服务器建立记录日志的索引
由于只向用户展示该用户的未读记录所以通过定时任务的形式定时将该用户的所有记录提前全量查询至搜索引擎中
推荐记录时同样查询出已读id集合，查询索引时设置搜索条件排除此id集合即可

```